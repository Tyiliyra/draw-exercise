// defining variables in global scope so they are usable by other functions without further ado
var canvas = document.getElementById("canvas");
canvas.width = window.innerWidth - document.getElementById("toolbar").offsetWidth - 8;
canvas.height = window.innerHeight - document.getElementById("heading").offsetHeight - 8;
var ctx = canvas.getContext('2d');
ctx.lineWidth = 1;
var currentShape = null;
var triangleVarient = null;
var vertexNum = null;
var isIrregularVisible = false;
var isFreehandVisible = false;
var freehandPath = [];
var freehandPathColor = [];
var myArray = [];
var guiContainerArray = [];
var z = null;
var countdownToStart = null;
var gameTime = null;
// var irregularShapePath = null;
freehand = false;
var mouseX = null;
var mouseY = null;
var pathInd = 0;
// var freehandPathArray = [];
var green = 0;
var greenToPathIndRatio = 0;
var finalScore = 0;
var gameDialog = document.getElementById("gameDialog");
var infoDialog = document.getElementById("infoDialog");

const refScaleX = 1204;
const refScaleY = 503;

// defining mouse coordinates
function  getMousePos(evt) {
    // alert(canvas.width + " " + canvas.height);
    var rect = canvas.getBoundingClientRect(); // abs. size of element
    var scaleX = canvas.width / rect.width;    // relationship bitmap vs. element for X
    var scaleY = canvas.height / rect.height;  // relationship bitmap vs. element for Y
    return {
      x: ( (evt.clientX || evt.changedTouches[0].clientX) - rect.left) * scaleX,   // scale mouse coordinates after they have
      y: ( (evt.clientY || evt.changedTouches[0].clientY) - rect.top) * scaleY     // been adjusted to be relative to element
    }
}

// defining freehand drawing
canvas.onmousedown = function(evt) {
    var pos = getMousePos(evt);
    mouseX = pos.x;
    mouseY = pos.y;

    freehand = true;
    // getting pixel array with rgb values
    var imageData = ctx.getImageData(mouseX, mouseY, 1, 1);
    var pixelArray = imageData.data;
    var color = "red";
    if (pixelArray[0] == 255 && pixelArray[1] <= 255 && pixelArray[2] == 255) {
        color = "green";
    }

    ctx.strokeStyle = color;
    ctx.lineWidth = 1;
    freehandPath[pathInd] = new Path2D();
    freehandPath[pathInd].moveTo(mouseX, mouseY);
    freehandPathColor[pathInd] = color;
    isFreehandVisible = true;
    return false;
}
canvas.ontouchstart = canvas.onmousedown;

canvas.onmousemove = function (evt) {
    var pos = getMousePos(evt);
    mouseX = pos.x;
    mouseY = pos.y;
    if (freehand == true) {
        var imageData = ctx.getImageData(mouseX, mouseY, 1, 1);
        var pixelArray = imageData.data;

        ctx.lineWidth = 1;
        if(pathInd >= 0) // in game after the initial countdown pathInd is set to -1 so it omits this if clause - not drawing the line to last remembered point before the pathInd reset
        {
            freehandPath[pathInd].lineTo(mouseX, mouseY);
            ctx.stroke(freehandPath[pathInd]);
        }

        if (pixelArray[0] == 255 && pixelArray[1] <= 255 && pixelArray[2] == 255) {
            green++
            pathInd++;
            freehandPath[pathInd] = new Path2D();
            freehandPath[pathInd].moveTo(mouseX, mouseY);
            freehandPathColor[pathInd] = "green";
            ctx.strokeStyle = "green";
        }
        else {
            pathInd++;
            freehandPath[pathInd] = new Path2D();
            freehandPath[pathInd].moveTo(mouseX, mouseY);
            freehandPathColor[pathInd] = "red";
            ctx.strokeStyle = "red";
        }

    }
    return false;
}
canvas.ontouchmove = canvas.onmousemove;

canvas.onmouseup = function() {
    freehand = false;
    return false;
}
canvas.ontouchend = canvas.onmouseup;

// defining function to return pseudo random values
function getRandom() {
    return (Math.random() * 3 + 1) * 100;
}

// defining function to clear canvas; somehow cleared canvas does not return the right pixel colors, the solution is to fillRect with those chosen one instead of clearing it
function clearAll() {
    ctx.fillStyle = "cornflowerblue";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
}

// defining new type (CircularShape)
function CircularShape(radius) {
    let scaleX = canvas.width / refScaleX;
    let scaleY = canvas.height / refScaleY;
    this.radius = radius * Math.min(scaleX, scaleY);
    this.draw = function() {
        ctx.strokeStyle = 'white';
        ctx.lineWidth = 5;
        ctx.beginPath();
        var x = 0.5 * canvas.width;
        var y = 0.5 * canvas.height;
        ctx.arc(x, y, this.radius, 0, 2 * Math.PI);
        ctx.stroke();
    }
}

// defining new type (RectangularShape)
function RectangularShape(farX, farY) {
    let scaleX = canvas.width / refScaleX;
    let scaleY = canvas.height / refScaleY;
    this.farX = farX * scaleX;
    this.farY = farY * scaleY;
    this.draw = function() {
        ctx.strokeStyle = 'white';
        ctx.lineWidth = 5;
        ctx.beginPath();
        var x = 0.5 * canvas.width;
        var y = 0.5 * canvas.height;
        ctx.moveTo(x - this.farX, y + this.farY);
        ctx.lineTo(x + this.farX, y + this.farY);
        ctx.lineTo(x + this.farX, y - this.farY);
        ctx.lineTo(x - this.farX, y - this.farY);
        ctx.closePath();
        ctx.stroke();
    }
}

// defining new type (TriangularShape)
function TriangularShape(moveToX, moveToY, lineTo1X, lineTo1Y, lineTo2X, lineTo2Y) {
    let scaleX = canvas.width / refScaleX;
    let scaleY = canvas.height / refScaleY;
    this.moveToX = moveToX * scaleX;
    this.moveToY = moveToY * scaleY;
    this.lineTo1X = lineTo1X * scaleX;
    this.lineTo1Y = lineTo1Y * scaleY;
    this.lineTo2X = lineTo2X * scaleX;
    this.lineTo2Y = lineTo2Y * scaleY;
    triangleVarient = Math.floor(Math.random() * 4 + 1);
    this.draw = function () {
        ctx.strokeStyle = 'white';
        ctx.lineWidth = 5;
        ctx.beginPath();
        var x = 0.5 * canvas.width;
        var y = 0.5 * canvas.height;
        if (triangleVarient == 1) {
            ctx.moveTo(x - this.moveToX, y + this.moveToY);
            ctx.lineTo(x + this.lineTo1X, y - this.lineTo1Y);
            ctx.lineTo(x - this.lineTo1X, y + this.lineTo1Y);
        }
        else if (triangleVarient == 2) {
            ctx.moveTo(x - this.moveToX, y + this.moveToY);
            ctx.lineTo(x - this.lineTo1X, y + this.lineTo1Y);
            ctx.lineTo(x + this.lineTo1X, y - this.lineTo1Y);
        }
        else if (triangleVarient == 3) {
            ctx.moveTo(x + this.moveToX, y - this.moveToY);
            ctx.lineTo(x + this.lineTo1X, y - this.lineTo1Y);
            ctx.lineTo(x - this.lineTo1X, y + this.lineTo1Y);
        }
        else {
            ctx.moveTo(x + this.moveToX, y - this.moveToY);
            ctx.lineTo(x - this.lineTo1X, y + this.lineTo1Y);
            ctx.lineTo(x + this.lineTo1X, y - this.lineTo1Y);
        }
        ctx.closePath();
        ctx.stroke();
    }
}

//defining new type (IrregularShape)
function IrregularShape() {
    let scaleX = canvas.width / refScaleX;
    let scaleY = canvas.height / refScaleY;
    this.draw = function() {
        vertexNum = Math.floor(Math.random() * 5 + 4);
        ctx.strokeStyle = 'white';
        ctx.lineWidth = 5;
        // var myArray = []; //remnants of array push method, which was initially an idea but after all Path2D is making the code less complex, this applies to the few other comments in this section below
        if (isIrregularVisible == false) {
            // ctx.beginPath();
            irregularShapePath = new Path2D;
            var x = 0.5 * canvas.width;
            var y = 0.5 * canvas.height;
            irregularShapePath.moveTo(x, y);
            for (var i = 0; i < vertexNum; i++) {
                var randX = -200 + Math.random() * 500;
                var randY = -200 + Math.random() * 500;
                irregularShapePath.lineTo(x + randX * scaleX, y + randY * scaleY);
            }
            // array.push(i++)
            irregularShapePath.closePath();
            ctx.stroke(irregularShapePath);
        }
        else {
            ctx.stroke(irregularShapePath);
        }
    }
}

// defining drawGrid function
function drawGrid() {
    var step = 0.1 * canvas.width;
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 1;
    if (canvas.width >= canvas.height) {
        step = 0.1 * canvas.height;
    }
    // var stepX = 0.1 * canvas.width;
    for (i=0; i<=canvas.width; i=i+step) {
        ctx.beginPath();
        ctx.moveTo (i, 0);
        ctx.lineTo (i, canvas.height);
        ctx.stroke();
    }
    // var stepY = 0.1 * canvas.height;
    for (j=0; j<=canvas.height; j=j+step) {
        ctx.beginPath();
        ctx.moveTo (0, j);
        ctx.lineTo (canvas.width, j);
        ctx.stroke();
    }
    document.getElementById("grid").gridVisible = true;
}

// adding anonymous function handler to our listener for our html element (id circle); inside it invokes already defined function
document.getElementById("circle").addEventListener("click", function() {
    clearAll();
    if (document.getElementById("grid").gridVisible == true) {
        drawGrid();
    }
    currentShape = new CircularShape(getRandom());
    currentShape.draw();
});

// adding anonymous function handler to our listener for our html element (id rectangle); inside it invokes already defined function
document.getElementById("rectangle").addEventListener("click", function() {
    clearAll();
    if (document.getElementById("grid").gridVisible == true) {
        drawGrid();
    }
    currentShape = new RectangularShape(getRandom(), getRandom());
    currentShape.draw();
});

// adding anonymous function handler to our listener for our html element (id triangle); inside it invokes already defined function
document.getElementById("triangle").addEventListener("click", function() {
    clearAll();
    if (document.getElementById("grid").gridVisible == true) {
        drawGrid();
    }
    currentShape = new TriangularShape(getRandom(), getRandom(), getRandom(), getRandom(), getRandom(), getRandom());
    currentShape.draw();
});

// adding anonymous function handler to our listener for our html element (id irregular); inside it invokes already defined function
document.getElementById("irregular").addEventListener("click", function() {
    clearAll();
    if (document.getElementById("grid").gridVisible == true) {
        drawGrid();
    }
    currentShape = new IrregularShape();
    currentShape.draw();
});

// assigning default value of grid visibility to false so it can be change to true while its actually there (drawn)
document.getElementById("grid").gridVisible = false;
// adding anonymous function handler to our listener for our html element (id grid); inside it invokes already defined function
document.getElementById("grid").addEventListener("click", function() {
    irregularVarient = 2
    if (this.gridVisible == false) {
        drawGrid();
    }
    else {
         clearAll();
         this.gridVisible = false;
    }
    if (currentShape != null) {
             //draw specific shape, which existed before the canvas clearing
            isIrregularVisible = true; 
            currentShape.draw();
            isIrregularVisible = false;
    }
    if (isFreehandVisible == true) {
        ctx.lineWidth = 1;
        for(let i = 0; i < pathInd; i++)
        {
            ctx.strokeStyle = freehandPathColor[i];
            ctx.stroke(freehandPath[i]);
        }
    }
});

// adding anonymous function handler to our listener for our html element (id clear); inside it invokes already defined function
document.getElementById("clear").addEventListener("click", function() {
    irregularVarient = 2
    clearAll();
    if (document.getElementById("grid").gridVisible == true) {
      drawGrid();
    }
    if (currentShape != null) {
            isIrregularVisible = true; 
            currentShape.draw();
            isIrregularVisible = false;
    }
    isFreehandVisible = false;
});

//
//
// code stuff behind the game functionality
//
//

function showGui() {
    guiContainerArray = document.querySelectorAll("#countdownContainer, #scoreContainer, #commandsContainer");
    for (var i = 0; i < guiContainerArray.length; i++) {
        guiContainerArray[i].classList.remove("guiContainerHidden");
        guiContainerArray[i].classList.add("guiContainerVisible");
    }
}

// rolling number 1 - 3 and drawing one of three shapes based on the result
function drawRandomShape() {
    clearAll() 
    var x = Math.floor(Math.random() * 3 + 1)
    if (x == 1) {
        currentShape = new CircularShape(getRandom())
        currentShape.draw();
    }
    else if (x == 2) {
        currentShape = new RectangularShape(getRandom(), getRandom());
        currentShape.draw();
    }
    else {
        currentShape = new TriangularShape(getRandom(), getRandom(), getRandom(), getRandom(), getRandom(), getRandom());
        currentShape.draw();
    }
    ctx.save();
}

// whole countdown in one place
function countdownWhole(isRestart) {
    if (document.getElementById("grid").gridVisible == true) {
        drawGrid();
    }
    document.getElementById("countdownContainer").classList.remove("countingGreen");
    document.getElementById("countdownContainer").classList.remove("countingRed");
    document.getElementById("countdownContainer").classList.add("countingYellow");
    document.getElementById("countdownContainer").innerHTML = "5";
    document.getElementById("scoreContainer").innerHTML = "SCORE: " + 0;
    finalScore = 0;
    var i = 5;
    clearInterval(countdownToStart);
    clearInterval(gameTime);
    if (isRestart) {
        countdownToStart = setInterval(function() {
            i--;
            document.getElementById("countdownContainer").innerHTML = i;
            if (i == -1) {
                document.getElementById("countdownContainer").classList.remove("countingYellow");
                document.getElementById("countdownContainer").classList.add("countingGreen");
                document.getElementById("countdownContainer").innerHTML = "GO";
                green = 0;
                pathInd = -1;
                clearAll();
                currentShape.draw();
                clearInterval(countdownToStart);
                var a = 31;
                gameTime = setInterval(function() {
                    a--;
                    greenToPathIndRatio = 0;
                    if(pathInd > 0)
                        greenToPathIndRatio = green / pathInd;
                    finalScore = Math.round(greenToPathIndRatio * 100);
                    finalScore = finalScore ? finalScore : 0; // if (variable == NaN) {variable = 0} does not work
                    document.getElementById("scoreContainer").innerHTML = "SCORE: " + finalScore;
                    
                    document.getElementById("countdownContainer").innerHTML = a;
                    if (a == 30) {
                        document.getElementById("countdownContainer").classList.remove("countingGreen");
                    }
                    if (a == 5) {
                        document.getElementById("countdownContainer").classList.add("countingRed");
                    }
                    if (a == 0) {
                        clearInterval(gameTime);
                        document.getElementById("countdownContainer").innerHTML = "THE END";
                    }
                }, 1000);   
            }
        }, 1000);
    }
}

document.getElementById("game").addEventListener("click", function() {
    showDialog(gameDialog);
})

document.getElementById("start").addEventListener("click", function() {
    closeDialog(gameDialog);
    showGui();
    drawRandomShape();
    countdownWhole(true);
})

// restart button simply restarts the game so its duration and score is reseted
document.getElementById("restartGame").addEventListener("click", function() {
    drawRandomShape();
    countdownWhole(true);
})

// endGame button simply exits the game, meaning it turns all of the guiContainer class elements to be hidden until the "reopening" process
document.getElementById("endGame").addEventListener("click", function() {
    countdownWhole(false);
    for (var i = 0; i < guiContainerArray.length; i++) {
        guiContainerArray[i].classList.remove("guiContainerVisible");
        guiContainerArray[i].classList.add("guiContainerHidden");
    }
})

// info button
document.getElementById("info").addEventListener("click", function() {
    showDialog(infoDialog);
    document.getElementById("close").addEventListener("click", function () {
        closeDialog(infoDialog);
    })
})

// exit button
document.getElementById("exit").addEventListener("click", function() {
    closeDialog(gameDialog);
})

//changing an image in game icon
document.getElementById("gameIcon").addEventListener("mouseenter", function() {
    document.getElementById("gameIcon").src = "GameIcon2.png";
})
document.getElementById("gameIcon").addEventListener("mouseleave", function() {
    document.getElementById("gameIcon").src = "GameIcon.png";
})

// separate function to show dialog (remove noDisplay class)
function showDialog(target) {
    target.classList.remove("noDisplay");
    // var children = target.children;
    // for (let i = 0, len = children.length; i < len; i++) {
    //     children[i].classList.remove("noDisplay");
    // }
}

// separate function to close dialog (add noDisplay class)
function closeDialog(target) {
    target.classList.add("noDisplay");
    // var children = target.children;
    // for (let i = 0, len = children.length; i < len; i++) {
    //     children[i].classList.add("noDisplay");
    // }
}